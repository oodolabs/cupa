import {assert} from "chai";
import * as decoders from "../src/decoders";

const PRINTED_PAGES = `
  [6 ENUMERATED]
	A4/LETTER	92
	LEGAL/FOLIO	0
	B5/EXECUTIVE	0
	ENVELOPE	0
	A5	0
	OTHER	0
`;

const PRINTER_STATUS = `
  CODE=62122
  DISPLAY="Paper Low Tray 1"
  ONLINE=TRUE
`;

const PRINTER_MULTI_STATUS = `
  CODE=62122
  CODE=40000
  DISPLAY="Paper Low Tray 1"
  ONLINE=TRUE
`;

describe('decoders', () => {

  it('number', () => {
    assert.equal(decoders.number(undefined), 0);
    assert.equal(decoders.number(null), 0);
    assert.equal(decoders.number(''), 0);
    assert.equal(decoders.number('0'), 0);
    assert.equal(decoders.number('123'), 123);
  });

  describe('kvlist', () => {
    it('should decode simple list', () => {
      const result = decoders.kvlist(1)(PRINTED_PAGES);
      assert.deepEqual(result, {
        "a4/letter": 92,
        "a5": 0,
        "b5/executive": 0,
        "envelope": 0,
        "legal/folio": 0,
        "other": 0
      });
    });

    it('should decode with equal', () => {
      const result = decoders.kvlist()(PRINTER_STATUS);
      assert.deepEqual(result, {
        "code": 62122,
        "display": "Paper Low Tray 1",
        "online": true
      });
    });

    it('should decode array list', () => {
      const result = decoders.kvlist()(PRINTER_MULTI_STATUS);
      assert.deepEqual(result, {
        "code": [62122, 40000],
        "display": "Paper Low Tray 1",
        "online": true
      });
    });

  });

});
