import {assert} from "chai";
import {findDriver} from "../src/drivers";

describe('drivers', () => {
  it.only('find driver with uri', () => {
    let found = findDriver('usb://Brother/HL-2595DW?serial=E78125A8N267473');
    assert.ok(found);
    found = findDriver('usb://Brother/H-L0000');
    assert.ok(found);
    found = findDriver('usb://HP/XXXXXX?serial=E78125A8N267473');
    assert.notOk(found);
  });
});
