# cupa

> Common Unix Printing Assistant

## Get Printer Status

```js
import {createAssistant} from "cupa";

(async () => {
  const assistant = createAssistant('usb://<Manufacturer>/<Product>?serial=<Serial Number>');
  console.log('- Status');
  console.log(await assistant.status());
})();
```
The result
```json5
// -->
{ 
  inklevel: 95, // percent
  drumleft: 11893, // papers left to print
  state: { 
    reasons: [ 'media-low', 'sleep' ], // state reason
    display: 'Paper Low Tray 1', // display message
  },
}
```

__State Reasons__

`State Reasons` is a set of reason belows:

* `ready`
* `pause`
* `wait`
* `toner-low`
* `ink-low`
* `job-cancel`
* `printing`
* `sleep`
* `toner-empty`
* `ink-empty`
* `media-jam` Paper Jam
* `media-empty` Paper empty
* `media-low` Paper low
* `<empty>` Empty state
* `<Number>` Unknown reason in code form.
