/// <reference types="node" />
import { EventEmitter } from "events";
import { Device, InEndpoint, OutEndpoint, Interface } from "usb";
import { CDI, Connection, PollCallback } from "../../defines";
interface UsbDeviceInfo {
    manufacturer: string;
    product: string;
    serial: string;
}
export declare class UsbDevice extends EventEmitter implements Connection {
    device: Device;
    info: UsbDeviceInfo;
    iface?: Interface;
    epi?: InEndpoint;
    epo?: OutEndpoint;
    _ready: Promise<UsbDevice>;
    _attached: Boolean;
    _opened: Boolean;
    constructor(device: Device, info: UsbDeviceInfo);
    protected _open(claim?: Boolean): Promise<this>;
    open(): Promise<this>;
    close(): Promise<this>;
    read(): Promise<string>;
    write(data: string | Buffer): Promise<any>;
    fetchStringDescriptor(id: any): Promise<string>;
    poll(cb: PollCallback): any;
    poll(timeout: number, cb: PollCallback): any;
    fetchDeviceId(): Promise<string>;
}
export declare function acquireUsbDevice(uri: string | CDI): Promise<UsbDevice | undefined>;
export declare function detect(uri: string | CDI): Promise<UsbDeviceInfo | undefined>;
export {};
