"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
const usb_1 = require("./usb");
const errors_1 = require("../../errors");
class UsbTransport {
    constructor(cdi) {
        this.cdi = cdi;
    }
    static detect(uri) {
        return usb_1.detect(uri);
    }
    connect() {
        return __awaiter(this, void 0, void 0, function* () {
            const conn = yield usb_1.acquireUsbDevice(this.cdi);
            if (!conn) {
                throw errors_1.ERR_DEVICE_NOT_FOUND(`with uri: ${this.cdi.uri}`);
            }
            return yield conn.open();
        });
    }
    fetchDeviceId() {
        return __awaiter(this, void 0, void 0, function* () {
            const conn = yield this.connect();
            try {
                return yield conn.fetchDeviceId();
            }
            finally {
                yield conn.close();
            }
        });
    }
}
module.exports = UsbTransport;
//# sourceMappingURL=index.js.map