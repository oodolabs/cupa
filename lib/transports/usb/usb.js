"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const events_1 = require("events");
const usb_1 = require("usb");
const PromiseA = require("bluebird");
const utils_1 = require("../../utils");
const debug = require('debug')('cupa:transport:usb');
const BUF_SIZE = 2048;
class UsbDevice extends events_1.EventEmitter {
    constructor(device, info) {
        super();
        this._attached = true;
        this.device = device;
        this.info = info;
    }
    _open(claim = false) {
        return __awaiter(this, void 0, void 0, function* () {
            if (this._opened) {
                return Promise.resolve(this);
            }
            this._opened = true;
            this.device.open();
            if (claim) {
                this.iface = this.device.interfaces[0];
                if (!this.iface) {
                    yield this.close();
                    throw new Error('The device is not a printer');
                }
                if (this.iface.isKernelDriverActive()) {
                    this.iface.detachKernelDriver();
                }
                try {
                    this.iface.claim();
                }
                catch (e) {
                    throw e;
                }
                this.epi = this.iface.endpoints.find(endpoint => endpoint.direction === 'in');
                if (!this.epi) {
                    yield this.close();
                    throw new Error('No in endpoint found in device');
                }
                this.epo = this.iface.endpoints.find(endpoint => endpoint.direction === 'out');
                if (!this.epo) {
                    yield this.close();
                    throw new Error('No out endpoint found in device');
                }
            }
            return this;
        });
    }
    open() {
        return __awaiter(this, void 0, void 0, function* () {
            return this._open(true);
        });
    }
    close() {
        return __awaiter(this, void 0, void 0, function* () {
            if (!this._opened) {
                return Promise.resolve(this);
            }
            if (this.iface) {
                yield utils_1.promiseFromCallback(cb => this.iface && this.iface.release(true, cb));
            }
            this.device.close();
            this.iface = undefined;
            this.epi = undefined;
            this.epo = undefined;
            this._opened = false;
            return this;
        });
    }
    read() {
        return __awaiter(this, void 0, void 0, function* () {
            if (!this.epi) {
                throw new Error('The device is not opened');
            }
            return new Promise((resolve, reject) => {
                const epi = this.epi;
                if (!epi) {
                    return reject(new Error('The device is not opened'));
                }
                let result;
                epi.on('data', data => {
                    if (data.length) {
                        result = data.toString();
                        epi.stopPoll();
                    }
                });
                epi.on('end', () => resolve(result));
                epi.startPoll();
            });
        });
    }
    write(data) {
        return __awaiter(this, void 0, void 0, function* () {
            if (!this.epo) {
                throw new Error('The device is not opened');
            }
            data = Buffer.isBuffer(data) ? data : Buffer.from(data);
            return utils_1.promiseFromCallback(cb => this.epo && this.epo.transferWithZLP(data, cb));
        });
    }
    fetchStringDescriptor(id) {
        return __awaiter(this, void 0, void 0, function* () {
            const data = yield utils_1.promiseFromCallback(cb => this.device.getStringDescriptor(id, cb));
            return data ? data.toString() : '';
        });
    }
    poll(timeoutOrCallback, callback) {
        if (typeof timeoutOrCallback === 'function') {
            callback = timeoutOrCallback;
            timeoutOrCallback = 0;
        }
        const timeout = timeoutOrCallback || 2000;
        const cb = callback;
        const epi = this.epi;
        if (!epi) {
            throw new Error('The device is not opened');
        }
        let canceled = false;
        return PromiseA.fromCallback(pcb => {
            epi.on('data', (data) => __awaiter(this, void 0, void 0, function* () {
                if (canceled)
                    return;
                cb(data, (result) => {
                    epi.stopPoll();
                    pcb(null, result);
                });
            }));
            epi.startPoll();
        }).timeout(timeout).catch(PromiseA.TimeoutError, e => {
            canceled = true;
            epi.stopPoll();
        });
    }
    fetchDeviceId() {
        return __awaiter(this, void 0, void 0, function* () {
            yield this.open();
            try {
                const device = this.device;
                const iface = this.iface;
                const data = yield utils_1.promiseFromCallback(cb => this.device.controlTransfer(usb_1.LIBUSB_REQUEST_TYPE_CLASS | usb_1.LIBUSB_ENDPOINT_IN | usb_1.LIBUSB_RECIPIENT_INTERFACE, 0, device.configDescriptor.iConfiguration, (iface.interfaceNumber << 8) | iface.altSetting, BUF_SIZE, cb));
                if (!data) {
                    throw new Error('fetch device id fail for control transfer returns null');
                }
                let len = data.readUInt16LE(0);
                if (len > BUF_SIZE || len < 14) {
                    len = data.readUInt16BE(0);
                }
                if (len > BUF_SIZE) {
                    len = BUF_SIZE;
                }
                if (len < 14) {
                    throw new Error('Invalid device ID');
                }
                return data.slice(2, len).toString();
            }
            finally {
                yield this.close();
            }
        });
    }
}
exports.UsbDevice = UsbDevice;
function fetchInfo(device) {
    return __awaiter(this, void 0, void 0, function* () {
        const answer = { manufacturer: '', product: '', serial: '' };
        try {
            yield device.open();
            try {
                answer.manufacturer = yield fetchStringDescriptor(device, device.deviceDescriptor.iManufacturer);
                answer.product = yield fetchStringDescriptor(device, device.deviceDescriptor.iProduct);
                answer.serial = yield fetchStringDescriptor(device, device.deviceDescriptor.iSerialNumber);
            }
            finally {
                yield device.close();
            }
        }
        catch (e) {
            if (/LIBUSB_ERROR_NOT_FOUND/g.test(e.message)) {
            }
            else {
                throw e;
            }
        }
        return answer;
    });
}
function fetchStringDescriptor(device, id) {
    return __awaiter(this, void 0, void 0, function* () {
        const data = yield utils_1.promiseFromCallback(cb => device.getStringDescriptor(id, cb));
        return data ? data.toString() : '';
    });
}
function acquireUsbDevice(uri) {
    return __awaiter(this, void 0, void 0, function* () {
        const cdi = typeof uri === 'string' ? utils_1.parseUri(uri) : uri;
        if (!cdi)
            return;
        const devices = usb_1.getDeviceList();
        for (const d of devices) {
            try {
                const info = yield fetchInfo(d);
                if (cdi.serial && !utils_1.equalsIgnoreCase(info.serial, cdi.serial)) {
                    continue;
                }
                if (!utils_1.equalsIgnoreCase(info.manufacturer, cdi.manufacturer) || !(utils_1.equalsIgnoreCase(info.product, cdi.product))) {
                    continue;
                }
                return new UsbDevice(d, info);
            }
            catch (e) {
            }
        }
    });
}
exports.acquireUsbDevice = acquireUsbDevice;
function detect(uri) {
    return __awaiter(this, void 0, void 0, function* () {
        const cdi = typeof uri === 'string' ? utils_1.parseUri(uri) : uri;
        if (!cdi)
            return;
        const devices = usb_1.getDeviceList();
        for (const d of devices) {
            try {
                const info = yield fetchInfo(d);
                if (cdi.serial && !utils_1.equalsIgnoreCase(info.serial, cdi.serial)) {
                    continue;
                }
                if (!utils_1.equalsIgnoreCase(info.manufacturer, cdi.manufacturer) || !(utils_1.equalsIgnoreCase(info.product, cdi.product))) {
                    continue;
                }
                return info;
            }
            catch (e) {
            }
        }
    });
}
exports.detect = detect;
//# sourceMappingURL=usb.js.map