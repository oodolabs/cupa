import { CDI, Connection, Transport } from "../../defines";
declare class UsbTransport implements Transport {
    protected cdi: CDI;
    static detect(uri: string | CDI): Promise<any>;
    constructor(cdi: CDI);
    connect(): Promise<Connection>;
    fetchDeviceId(): Promise<string>;
}
export = UsbTransport;
