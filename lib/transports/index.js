"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const utils_1 = require("../utils");
const transports = {
    usb: require('./usb')
};
function acquireTransport(uri, opts) {
    const cdi = typeof uri === 'string' ? utils_1.parseUri(uri) : uri;
    if (!cdi) {
        throw new Error('Invalid uri to parse as CDI: ' + uri);
    }
    const TransportClass = transports[cdi.protocol];
    if (TransportClass) {
        return new TransportClass(cdi, opts);
    }
}
exports.acquireTransport = acquireTransport;
function detect(uri) {
    return __awaiter(this, void 0, void 0, function* () {
        const cdi = typeof uri === 'string' ? utils_1.parseUri(uri) : uri;
        if (!cdi) {
            throw new Error('Invalid uri to parse as CDI: ' + uri);
        }
        const TransportClass = transports[cdi.protocol];
        if (TransportClass.detect) {
            return (yield TransportClass.detect(cdi)) || false;
        }
    });
}
exports.detect = detect;
//# sourceMappingURL=index.js.map