import { CDI, Transport } from "../defines";
export declare function acquireTransport(uri: string | CDI, opts?: {
    [name: string]: any;
}): Transport | undefined;
export declare function detect(uri: string | CDI): Promise<any | false | undefined>;
