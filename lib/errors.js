"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const errs = require("errs");
function ERR_DEVICE_NOT_FOUND(identifier) {
    return errs.create({
        code: 'DEVICE_NOT_FOUND',
        status: 404,
        statusCode: 404,
        message: `Device not found${identifier ? ' ' + identifier : '.'}`
    });
}
exports.ERR_DEVICE_NOT_FOUND = ERR_DEVICE_NOT_FOUND;
function ERR_DIRECTIVE_NOT_SUPPORT(directive) {
    return errs.create({
        code: 'NOT_SUPPORT',
        status: 40011,
        statusCode: 40011,
        message: `The directive \`${directive}\` is no supported`
    });
}
exports.ERR_DIRECTIVE_NOT_SUPPORT = ERR_DIRECTIVE_NOT_SUPPORT;
//# sourceMappingURL=errors.js.map