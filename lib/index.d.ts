import { Assistant } from "./assistant";
export * from "./defines";
export * from "./assistant";
export declare function createAssistant(uri: any, opts?: any): Assistant;
export declare function detect(uri: string): Promise<any | false | undefined>;
