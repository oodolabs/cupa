/// <reference types="node" />
export interface KVListOptions {
    splitter?: RegExp;
    intercept?: (key: any, value: any) => [string, any];
}
export declare function kvlist(s: string | Buffer, start?: number, options?: KVListOptions): {};
