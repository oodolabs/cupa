"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const JSON5 = require("json5");
function kvlist(s, start = 0, options = {}) {
    const splitter = options.splitter || /(^[^:= \t]+)[:= \t](.+)/;
    const intercept = options.intercept || ((k, v) => [k, v]);
    s = Buffer.isBuffer(s) ? s.toString() : s;
    return s.trim().split('\n').map(l => l.trim()).splice(start)
        .map(l => {
        const matched = splitter.exec(l);
        if (matched) {
            return [matched[1].toLowerCase(), matched[2]];
        }
    })
        .reduce((r, parts) => {
        if (parts && parts.length === 2) {
            let [k, v] = intercept(parts[0], parts[1]);
            try {
                v = JSON5.parse(v);
            }
            catch (e) {
            }
            if (v === 'TRUE' || v === 'ON' || v === 'on') {
                v = true;
            }
            else if (v === 'FALSE' || v === 'OFF' || v === 'off') {
                v = false;
            }
            if (k in r) {
                if (!Array.isArray(r[k])) {
                    r[k] = [r[k]];
                }
                r[k].push(v);
            }
            else {
                r[k] = v;
            }
        }
        return r;
    }, {});
}
exports.kvlist = kvlist;
//# sourceMappingURL=decoders.js.map