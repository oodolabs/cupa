"use strict";
function __export(m) {
    for (var p in m) if (!exports.hasOwnProperty(p)) exports[p] = m[p];
}
Object.defineProperty(exports, "__esModule", { value: true });
const assistant_1 = require("./assistant");
const t = require("./transports");
__export(require("./assistant"));
function createAssistant(uri, opts) {
    return assistant_1.Assistant.create(uri);
}
exports.createAssistant = createAssistant;
function detect(uri) {
    return t.detect(uri);
}
exports.detect = detect;
//# sourceMappingURL=index.js.map