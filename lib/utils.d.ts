import { CDI } from "./defines";
export declare function promiseFromCallback<T>(fn: (cb: any) => void): Promise<T>;
export declare function parseUri(uri: string): CDI | undefined;
export declare function equalsIgnoreCase(str1: any, str2: any): boolean;
