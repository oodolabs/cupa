/// <reference types="node" />
export interface Directive<T> {
    code: (...args: any[]) => string;
    decode?: (data: string) => T;
}
export interface Props {
    [name: string]: any;
}
export interface Directives {
    inklevel?: Directive<number>;
    drumleft?: Directive<number>;
    pages?: Directive<Props>;
    status?: Directive<Props>;
    multistatus?: Directive<Props>;
    tray?: Directive<void>;
    autosleep?: Directive<void>;
    deepsleep?: Directive<void>;
    pagenotify?: Directive<void>;
    jobnotify?: Directive<void>;
}
export declare type Reason = '' | 'ready' | 'pause' | 'wait' | 'toner-low' | 'ink-low' | 'job-cancel' | 'printing' | 'sleep' | 'toner-empty' | 'ink-empty' | 'media-jam' | 'media-empty' | 'media-low';
export declare type Reasons = Reason[];
export interface Status {
    inklevel: number;
    drumleft: number;
    states: {
        reasons: Reasons;
        display?: string;
    };
}
export interface Driver {
    rawstatus(): Promise<Props>;
    status(): Promise<Status>;
}
export interface DriverClass {
    manufacturer: string;
    models: string;
    new (transport: Transport): Driver;
}
export interface CDI {
    uri: string;
    protocol: string;
    manufacturer: string;
    product: string;
    serial?: string;
}
export interface PollCallback {
    (data: string, resolve: (data: any) => void): any;
}
export interface Connection {
    open(): Promise<this>;
    close(): Promise<this>;
    read(): Promise<string>;
    write(data: string | Buffer): Promise<void>;
    poll(cb: PollCallback): any;
    poll(timeout: number, cb: PollCallback): any;
    fetchDeviceId(): Promise<string>;
}
export interface Transport {
    connect(): Promise<Connection>;
    fetchDeviceId(): Promise<string>;
}
