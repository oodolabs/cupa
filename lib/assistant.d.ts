import { Driver, Props, CDI, Status } from "./defines";
export declare class Assistant {
    protected cdi: CDI;
    protected driver: Driver;
    static create(uri: string | CDI): Assistant;
    constructor(cdi: CDI, driver: Driver);
    rawstatus(): Promise<Props>;
    status(): Promise<Status>;
}
