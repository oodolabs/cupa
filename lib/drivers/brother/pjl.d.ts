import { Directive, Driver, Props, Status, Transport } from "../../defines";
export declare class PJL implements Driver {
    protected transport: Transport;
    static readonly manufacturer: string;
    static readonly models: string[];
    constructor(transport: Transport);
    fetch(cmds: {
        [name: string]: Directive<any>;
    }): Promise<Props>;
    rawstatus(): Promise<Props>;
    status(): Promise<Status>;
}
