"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const ascii_1 = require("../../ascii");
const decoders_1 = require("../../decoders");
const STATES = {
    10001: 'ready',
    10002: 'pause',
    10003: 'wait',
    10006: 'toner-low',
    10007: 'job-cancel',
    10023: 'printing',
    40000: 'sleep',
    40010: 'toner-empty',
    41000: 'media-empty',
    41200: 'media-empty',
    41300: 'media-empty',
    41913: 'media-empty',
    44102: 'media-jam',
    60148: 'media-empty',
    62122: 'media-low',
    62123: 'media-low',
    62124: 'media-low',
    62125: 'media-low',
    62126: 'media-low',
    65001: "media-jam",
    65002: "media-jam",
    65004: "media-jam",
    65016: "media-jam",
    65032: "media-jam",
    65064: "media-jam",
};
const codes = {
    inklevel: () => `${ascii_1.default.ESC}%-12345X@PJL${ascii_1.default.CR}${ascii_1.default.LF}@PJL DINQUIRE REMAINTONER${ascii_1.default.CR}${ascii_1.default.LF}${ascii_1.default.ESC}%-12345X`,
    drumleft: () => `${ascii_1.default.ESC}%-12345X@PJL${ascii_1.default.CR}${ascii_1.default.LF}@PJL DINQUIRE NEXTCAREDRM${ascii_1.default.CR}${ascii_1.default.LF}${ascii_1.default.ESC}%-12345X`,
    pages: () => `${ascii_1.default.ESC}%-12345X@PJL${ascii_1.default.CR}${ascii_1.default.LF}@PJL INFO PAGES${ascii_1.default.CR}${ascii_1.default.LF}${ascii_1.default.ESC}%-12345X`,
    status: () => `${ascii_1.default.ESC}%-12345X@PJL${ascii_1.default.CR}${ascii_1.default.LF}@PJL INFO STATUS${ascii_1.default.CR}${ascii_1.default.LF}${ascii_1.default.ESC}%-12345X`,
    multistatus: () => `${ascii_1.default.ESC}%-12345X@PJL${ascii_1.default.CR}${ascii_1.default.LF}@PJL INFO MULTISTATUS${ascii_1.default.CR}${ascii_1.default.LF}${ascii_1.default.ESC}%-12345X`,
    tray: (source) => `${ascii_1.default.ESC}%-12345X@PJL${ascii_1.default.CR}${ascii_1.default.LF}@PJL DEFAULT SOURCETRAY=TRAY${source === 'auto' ? 0 : source}${ascii_1.default.CR}${ascii_1.default.LF}${ascii_1.default.ESC}%-12345X`,
    autosleep: (enabled) => `${ascii_1.default.ESC}%-12345X@PJL${ascii_1.default.CR}${ascii_1.default.LF}@PJL DEFAULT AUTOSLEEP=${enabled ? 'ON' : 'OFF'}${ascii_1.default.CR}${ascii_1.default.LF}${ascii_1.default.ESC}%-12345X`,
    deepsleep: (enabled) => `${ascii_1.default.ESC}%-12345X@PJL${ascii_1.default.CR}${ascii_1.default.LF}@PJL DEFAULT DEEPSLEEP=${enabled ? 'ON' : 'OFF'}${ascii_1.default.CR}${ascii_1.default.LF}${ascii_1.default.ESC}%-12345X`,
    pagenotify: (enabled) => `${ascii_1.default.ESC}%-12345X@PJL${ascii_1.default.CR}${ascii_1.default.LF}@PJL USTATUS PAGE=${enabled ? 'ON' : 'OFF'}${ascii_1.default.CR}${ascii_1.default.LF}${ascii_1.default.ESC}%-12345X`,
    jobnotify: (enabled) => `${ascii_1.default.ESC}%-12345X@PJL${ascii_1.default.CR}${ascii_1.default.LF}@PJL USTATUS JOB=${enabled ? 'ON' : 'OFF'}${ascii_1.default.CR}${ascii_1.default.LF}${ascii_1.default.ESC}%-12345X`,
};
const DIRECTIVES = {
    inklevel: {
        code: codes.inklevel,
        decode: data => {
            const matched = data.toString().match(/@PJL DINQUIRE REMAINTONER[\r\n]*([\d]+)/);
            if (matched) {
                return parseInt(matched[1]);
            }
        }
    },
    drumleft: {
        code: codes.drumleft,
        decode: data => {
            const matched = data.toString().match(/@PJL DINQUIRE NEXTCAREDRM[\r\n]*([\d]+)/);
            if (matched) {
                return parseInt(matched[1]);
            }
        }
    },
    multistatus: {
        code: codes.multistatus,
        decode: data => {
            const matched = data.toString().match(/@PJL INFO MULTISTATUS/);
            if (matched) {
                return decoders_1.kvlist(data, 1);
            }
        }
    }
};
const STATUS_DIRECTIVES = {
    inklevel: {
        code: codes.inklevel,
        decode: data => {
            const matched = data.toString().match(/@PJL DINQUIRE REMAINTONER[\r\n]*([\d]+)/);
            if (matched) {
                return parseInt(matched[1]);
            }
        }
    },
    drumleft: {
        code: codes.drumleft,
        decode: data => {
            const matched = data.toString().match(/@PJL DINQUIRE NEXTCAREDRM[\r\n]*([\d]+)/);
            if (matched) {
                return parseInt(matched[1]);
            }
        }
    },
    state: {
        code: codes.multistatus,
        decode: data => {
            const matched = data.toString().match(/@PJL INFO MULTISTATUS/);
            if (matched) {
                const result = decoders_1.kvlist(data, 1);
                let codes = result['code'];
                if (!Array.isArray(codes)) {
                    codes = [codes];
                }
                return {
                    reasons: codes.map(code => STATES[code] || code),
                    display: result['display'],
                };
            }
        }
    }
};
class PJL {
    constructor(transport) {
        this.transport = transport;
    }
    fetch(cmds) {
        return __awaiter(this, void 0, void 0, function* () {
            const result = {};
            const names = Object.keys(cmds);
            const conn = yield this.transport.connect();
            let resolved = false;
            try {
                const polling = conn.poll((data, resolve) => {
                    if (!data || !data.length)
                        return;
                    for (const name of names) {
                        const decode = cmds[name].decode;
                        if (decode) {
                            const decoded = decode(data);
                            if (decoded != null) {
                                result[name] = decoded;
                                names.splice(names.indexOf(name), 1);
                                break;
                            }
                        }
                    }
                    if (!names.length && !resolved) {
                        resolved = true;
                        resolve(result);
                    }
                });
                for (const name of names) {
                    yield conn.write(cmds[name].code());
                }
                yield polling;
                return result;
            }
            catch (e) {
                throw e;
            }
            finally {
                yield conn.close();
            }
        });
    }
    rawstatus() {
        return __awaiter(this, void 0, void 0, function* () {
            return yield this.fetch(DIRECTIVES);
        });
    }
    status() {
        return __awaiter(this, void 0, void 0, function* () {
            return yield this.fetch(STATUS_DIRECTIVES);
        });
    }
}
PJL.manufacturer = 'Brother';
PJL.models = [
    ...["HL-1118", "HL-1208", "HL-1218W", "HL-1850", "HL-1870N"],
    'HL-2*',
    'HL-5*',
    'HL-6*',
    'HL-B2*',
    'DCP-1*',
    'DCP-7*',
    'DCP-8*',
    'DCP-B7*',
    'MFC-1*',
    'MFC-7*',
    'MFC-8*',
    'MFC-B7*',
    'FAX-2*',
];
exports.PJL = PJL;
//# sourceMappingURL=pjl.js.map