"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const minimatch = require("minimatch");
const utils_1 = require("../utils");
const DriverClasses = [
    ...require('./brother')
];
exports.default = DriverClasses;
function findDriver(uri) {
    const cdi = typeof uri === 'string' ? utils_1.parseUri(uri) : uri;
    if (!cdi)
        return;
    return DriverClasses.find(DC => {
        if (DC.manufacturer.toLowerCase() !== cdi.manufacturer) {
            return false;
        }
        const supported = Array.isArray(DC.models) ? DC.models : [DC.models];
        for (const m of supported) {
            if (minimatch(cdi.product, m)) {
                return true;
            }
        }
        return false;
    });
}
exports.findDriver = findDriver;
//# sourceMappingURL=index.js.map