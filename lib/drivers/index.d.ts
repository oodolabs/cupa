import { CDI, DriverClass } from "../defines";
declare const DriverClasses: DriverClass[];
export default DriverClasses;
export declare function findDriver(uri: string | CDI): DriverClass | undefined;
