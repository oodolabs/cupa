"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const url_1 = require("url");
const qs = require("querystring");
function promiseFromCallback(fn) {
    return new Promise(function (resolve, reject) {
        fn(function (err, data) {
            if (err)
                return reject(err);
            resolve(data);
        });
    });
}
exports.promiseFromCallback = promiseFromCallback;
function parseUri(uri) {
    const url = url_1.parse(uri);
    if (!url.protocol || !url.host || !url.pathname) {
        return;
    }
    const protocol = url.protocol.split(':').filter(s => s.trim())[0];
    const manufacturer = decodeURI(url.host.split('/').filter(s => s.trim())[0]);
    const product = decodeURI(url.pathname.split('/').filter(s => s.trim())[0]);
    const opts = url.query ? qs.parse(url.query) : {};
    const serial = decodeURI(opts['serial']);
    return { uri, protocol, manufacturer, product, serial };
}
exports.parseUri = parseUri;
function equalsIgnoreCase(str1, str2) {
    if (str1 === str2 && str1 == null) {
        return true;
    }
    if ((str1 && !str2) || (!str1 && str2)) {
        return false;
    }
    return str1.toLowerCase() === str2.toLowerCase();
}
exports.equalsIgnoreCase = equalsIgnoreCase;
//# sourceMappingURL=utils.js.map