"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const drivers_1 = require("./drivers");
const transports_1 = require("./transports");
const utils_1 = require("./utils");
class Assistant {
    static create(uri) {
        const cdi = typeof uri === 'string' ? utils_1.parseUri(uri) : uri;
        if (!cdi) {
            throw new Error('Invalid uri to parse as CDI: ' + uri);
        }
        const Driver = drivers_1.findDriver(uri);
        if (!Driver) {
            throw new Error('No driver found for uri: ' + uri);
        }
        const transport = transports_1.acquireTransport(cdi);
        if (!transport) {
            throw new Error('No transport found for uri: ' + uri);
        }
        return new this(cdi, new Driver(transport));
    }
    constructor(cdi, driver) {
        this.cdi = cdi;
        this.driver = driver;
    }
    rawstatus() {
        return __awaiter(this, void 0, void 0, function* () {
            return this.driver.rawstatus();
        });
    }
    status() {
        return __awaiter(this, void 0, void 0, function* () {
            return this.driver.status();
        });
    }
}
exports.Assistant = Assistant;
//# sourceMappingURL=assistant.js.map