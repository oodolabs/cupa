import {Assistant} from "./assistant";
import * as t from "./transports";

export * from "./defines";
export * from "./assistant";

export function createAssistant(uri, opts?) {
  return Assistant.create(uri);
}

export function detect(uri: string): Promise<any | false | undefined> {
  return t.detect(uri);
}
