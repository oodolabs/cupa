import {CDI} from "./defines";
import {parse as parseUrl} from "url";
import * as qs from "querystring";

export function promiseFromCallback<T>(fn: (cb) => void): Promise<T> {
  return new Promise(function (resolve, reject) {
    fn(function (err, data) {
      if (err)
        return reject(err);
      resolve(data);
    });
  })
}

export function parseUri(uri: string): CDI | undefined {
  const url = parseUrl(uri);
  if (!url.protocol || !url.host || !url.pathname) {
    return;
  }

  const protocol = url.protocol.split(':').filter(s => s.trim())[0];
  const manufacturer = decodeURI(url.host.split('/').filter(s => s.trim())[0]);
  const product = decodeURI(url.pathname.split('/').filter(s => s.trim())[0]);
  const opts = url.query ? qs.parse(<string>url.query) : {};
  const serial = decodeURI(<string> opts['serial']);
  return {uri, protocol, manufacturer, product, serial};
}

export function equalsIgnoreCase(str1, str2) {
  if (str1 === str2 && str1 == null) {
    return true;
  }

  if ((str1 && !str2) || (!str1 && str2)) {
    return false;
  }

  return str1.toLowerCase() === str2.toLowerCase();
}
