import {CDI, Transport} from "../defines";
import {parseUri} from "../utils";

const transports = {
  usb: require('./usb')
};

export function acquireTransport(uri: string | CDI, opts?: {[name: string]: any}): Transport | undefined {
  const cdi = typeof uri === 'string' ? parseUri(uri) : uri;
  if (!cdi) {
    throw new Error('Invalid uri to parse as CDI: ' + uri);
  }
  const TransportClass = transports[cdi.protocol];
  if (TransportClass) {
    return new TransportClass(cdi, opts);
  }
}

export async function detect(uri: string | CDI): Promise<any | false | undefined> {
  const cdi = typeof uri === 'string' ? parseUri(uri) : uri;
  if (!cdi) {
    throw new Error('Invalid uri to parse as CDI: ' + uri);
  }
  const TransportClass = transports[cdi.protocol];
  if (TransportClass.detect) {
    return (await TransportClass.detect(cdi)) || false;
  }
}
