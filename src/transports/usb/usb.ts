import {EventEmitter} from "events";
import {
  Device,
  InEndpoint,
  OutEndpoint,
  Interface,
  LIBUSB_REQUEST_TYPE_CLASS,
  LIBUSB_ENDPOINT_IN, LIBUSB_RECIPIENT_INTERFACE, getDeviceList
} from "usb";
import * as PromiseA from "bluebird";
import {equalsIgnoreCase, parseUri, promiseFromCallback} from "../../utils";
import {CDI, Connection, PollCallback} from "../../defines";

const debug = require('debug')('cupa:transport:usb');

const BUF_SIZE = 2048;

interface UsbDeviceInfo {
  manufacturer: string;
  product: string;
  serial: string;
}

export class UsbDevice extends EventEmitter implements Connection {
  device: Device;
  info: UsbDeviceInfo;

  iface?: Interface;
  epi?: InEndpoint;
  epo?: OutEndpoint;

  _ready: Promise<UsbDevice>;
  _attached: Boolean = true;
  _opened: Boolean;

  constructor(device: Device, info: UsbDeviceInfo) {
    super();
    this.device = device;
    this.info = info;
  }

  protected async _open(claim: Boolean = false) {
    if (this._opened) {
      return Promise.resolve(this);
    }
    this._opened = true;

    this.device.open();

    if (claim) {
      this.iface = this.device.interfaces[0];
      if (!this.iface) {
        await this.close();
        throw new Error('The device is not a printer');
      }
      if (this.iface.isKernelDriverActive()) {
        this.iface.detachKernelDriver();
      }

      try {
        this.iface.claim();
      } catch (e) {
        throw e;
      }

      this.epi = <InEndpoint> this.iface.endpoints.find(endpoint => endpoint.direction === 'in');
      if (!this.epi) {
        await this.close();
        throw new Error('No in endpoint found in device');
      }
      this.epo = <OutEndpoint> this.iface.endpoints.find(endpoint => endpoint.direction === 'out');
      if (!this.epo) {
        await this.close();
        throw new Error('No out endpoint found in device');
      }
    }

    return this;
  }

  async open(): Promise<this> {
    return this._open(true);
  }

  async close(): Promise<this> {
    if (!this._opened) {
      return Promise.resolve(this);
    }

    if (this.iface) {
      await promiseFromCallback(cb => this.iface && this.iface.release(true, cb));
    }

    this.device.close();

    this.iface = undefined;
    this.epi = undefined;
    this.epo = undefined;
    this._opened = false;
    return this;
  }

  async read(): Promise<string> {
    if (!this.epi) {
      throw new Error('The device is not opened');
    }
    return new Promise<string>((resolve, reject) => {
      const epi = this.epi;
      if (!epi) {
        return reject(new Error('The device is not opened'));
      }

      let result;
      epi.on('data', data => {
        if (data.length) {
          result = data.toString();
          epi.stopPoll();
        }
      });
      epi.on('end', () => resolve(result));
      epi.startPoll();
    });
  }

  async write(data: string | Buffer): Promise<any> {
    if (!this.epo) {
      throw new Error('The device is not opened');
    }
    data = Buffer.isBuffer(data) ? data : Buffer.from(data);
    return promiseFromCallback(cb => this.epo && this.epo.transferWithZLP(<Buffer> data, cb));
  }

  async fetchStringDescriptor(id): Promise<string> {
    const data = await promiseFromCallback<Buffer>(cb => this.device.getStringDescriptor(id, cb));
    return data ? data.toString() : '';
  }

  poll(cb: PollCallback);
  poll(timeout: number, cb: PollCallback);
  poll(timeoutOrCallback: number | PollCallback, callback?: PollCallback) {
    if (typeof timeoutOrCallback === 'function') {
      callback = timeoutOrCallback;
      timeoutOrCallback = 0;
    }
    const timeout = timeoutOrCallback || 2000;
    const cb = <PollCallback> callback;

    const epi = this.epi;
    if (!epi) {
      throw new Error('The device is not opened');
    }

    let canceled = false;

    return PromiseA.fromCallback(pcb => {
      epi.on('data', async data => {
        if (canceled) return;
        cb(data, (result) => {
          epi.stopPoll();
          pcb(null, result);
        });
      });
      epi.startPoll();
    }).timeout(timeout).catch(PromiseA.TimeoutError, e => {
      canceled = true;
      epi.stopPoll();
    });
  }

  async fetchDeviceId(): Promise<string> {
    await this.open();
    try {
      const device = <Device>this.device;
      const iface = <Interface>this.iface;
      const data: Buffer = await promiseFromCallback<Buffer>(cb => this.device.controlTransfer(
        LIBUSB_REQUEST_TYPE_CLASS | LIBUSB_ENDPOINT_IN | LIBUSB_RECIPIENT_INTERFACE,
        0,
        device.configDescriptor.iConfiguration,
        (iface.interfaceNumber << 8) | iface.altSetting,
        BUF_SIZE,
        cb));

      if (!data) {
        throw new Error('fetch device id fail for control transfer returns null');
      }

      let len = data.readUInt16LE(0);
      if (len > BUF_SIZE || len < 14) {
        len = data.readUInt16BE(0);
      }

      if (len > BUF_SIZE) {
        len = BUF_SIZE;
      }

      if (len < 14) {
        throw new Error('Invalid device ID');
      }

      return data.slice(2, len).toString();
    } finally {
      await this.close();
    }
  }
}

async function fetchInfo(device: Device): Promise<UsbDeviceInfo> {
  const answer: UsbDeviceInfo = {manufacturer: '', product: '', serial: ''};
  try {
    await device.open();
    try {
      answer.manufacturer = await fetchStringDescriptor(device, device.deviceDescriptor.iManufacturer);
      answer.product = await fetchStringDescriptor(device, device.deviceDescriptor.iProduct);
      answer.serial = await fetchStringDescriptor(device, device.deviceDescriptor.iSerialNumber);
    } finally {
      await device.close();
    }
  } catch (e) {
    if (/LIBUSB_ERROR_NOT_FOUND/g.test(e.message)) {
      // ignore
    } else {
      throw e;
    }
  }

  return answer;
}

async function fetchStringDescriptor(device, id): Promise<string> {
  const data = await promiseFromCallback<Buffer>(cb => device.getStringDescriptor(id, cb));
  return data ? data.toString() : '';
}

export async function acquireUsbDevice(uri: string | CDI): Promise<UsbDevice | undefined> {
  const cdi = typeof uri === 'string' ? parseUri(uri) : <CDI>uri;
  if (!cdi) return;
  const devices = getDeviceList();
  for (const d of devices) {
    try {
      const info = await fetchInfo(d);
      if (cdi.serial && !equalsIgnoreCase(info.serial, cdi.serial)) {
        continue;
      }
      if (!equalsIgnoreCase(info.manufacturer, cdi.manufacturer) || !(equalsIgnoreCase(info.product, cdi.product))) {
        continue;
      }
      return new UsbDevice(d, info);
    } catch (e) {
    }
  }
}

export async function detect(uri: string | CDI): Promise<UsbDeviceInfo | undefined> {
  const cdi = typeof uri === 'string' ? parseUri(uri) : <CDI>uri;
  if (!cdi) return;
  const devices = getDeviceList();
  for (const d of devices) {
    try {
      const info = await fetchInfo(d);
      if (cdi.serial && !equalsIgnoreCase(info.serial, cdi.serial)) {
        continue;
      }
      if (!equalsIgnoreCase(info.manufacturer, cdi.manufacturer) || !(equalsIgnoreCase(info.product, cdi.product))) {
        continue;
      }
      return info;
    } catch (e) {
    }
  }
}
