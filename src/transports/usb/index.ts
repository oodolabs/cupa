import {acquireUsbDevice, detect} from "./usb";
import {CDI, Connection, Transport} from "../../defines";
import {ERR_DEVICE_NOT_FOUND} from "../../errors";

class UsbTransport implements Transport {

  static detect(uri: string | CDI): Promise<any> {
    return detect(uri);
  }

  constructor(protected cdi: CDI) {
  }

  async connect(): Promise<Connection> {
    const conn = await acquireUsbDevice(this.cdi);
    if (!conn) {
      throw ERR_DEVICE_NOT_FOUND(`with uri: ${this.cdi.uri}`)
    }
    return await conn.open();
  }

  async fetchDeviceId(): Promise<string> {
    const conn = await this.connect();
    try {
      return await conn.fetchDeviceId();
    } finally {
      await conn.close();
    }
  }


}

export = UsbTransport;
