import {Directives, Driver, Props, CDI, Status, Reason} from "./defines";
import {findDriver} from "./drivers";
import {acquireTransport} from "./transports";
import {parseUri} from "./utils";

export class Assistant {

  protected cdi: CDI;
  protected driver: Driver;

  static create(uri: string | CDI) {
    const cdi = typeof uri === 'string' ? parseUri(uri) : uri;
    if (!cdi) {
      throw new Error('Invalid uri to parse as CDI: ' + uri);
    }
    const Driver = findDriver(uri);
    if (!Driver) {
      throw new Error('No driver found for uri: ' + uri);
    }
    const transport = acquireTransport(cdi);
    if (!transport) {
      throw new Error('No transport found for uri: ' + uri);
    }
    return new this(cdi, new Driver(transport));
  }

  constructor(cdi: CDI, driver: Driver) {
    this.cdi = cdi;
    this.driver = driver;
  }

  async rawstatus(): Promise<Props> {
    return this.driver.rawstatus();
  }

  async status(): Promise<Status> {
    return this.driver.status();
  }

}
