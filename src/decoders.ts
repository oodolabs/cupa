import * as JSON5 from "json5";

export interface KVListOptions {
  splitter?: RegExp;
  intercept?: (key, value) => [string, any]
}

export function kvlist(s: string | Buffer, start: number = 0, options: KVListOptions = {}) {
  const splitter = options.splitter || /(^[^:= \t]+)[:= \t](.+)/;
  const intercept = options.intercept || ((k, v) => [k, v]);
  s = Buffer.isBuffer(s) ? s.toString() : s;
  return s.trim().split('\n').map(l => l.trim()).splice(start)
    .map(l => {
      const matched = splitter.exec(l);
      if (matched) {
        return [matched[1].toLowerCase(), matched[2]];
      }
    })
    .reduce((r, parts) => {
      if (parts && parts.length === 2) {
        let [k, v] = intercept(parts[0], parts[1]);
        try {
          v = JSON5.parse(v);
        } catch (e) {
          // ignore
        }

        // transform boolean
        if (v === 'TRUE' || v === 'ON' || v === 'on') {
          v = true;
        } else if (v === 'FALSE' || v === 'OFF' || v === 'off') {
          v = false
        }

        if (k in r) { // already has a `k` property
          if (!Array.isArray(r[k])) { // not an array
            r[k] = [r[k]] // convert to array
          }
          r[k].push(v); // push current value
        } else {
          r[k] = v;
        }
      }
      return r;
    }, {});
}
