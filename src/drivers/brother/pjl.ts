import ascii from "../../ascii";
import {kvlist} from "../../decoders";
import {Directive, Driver, Props, Status, Transport} from "../../defines";

const STATES = {
  10001: 'ready',
  10002: 'pause',
  10003: 'wait',
  10006: 'toner-low',
  10007: 'job-cancel',
  10023: 'printing',
  40000: 'sleep',
  40010: 'toner-empty',
  41000: 'media-empty',
  41200: 'media-empty',
  41300: 'media-empty',
  41913: 'media-empty',
  44102: 'media-jam',
  60148: 'media-empty',
  62122: 'media-low',
  62123: 'media-low',
  62124: 'media-low',
  62125: 'media-low',
  62126: 'media-low',
  65001: "media-jam",
  65002: "media-jam",
  65004: "media-jam",
  65016: "media-jam",
  65032: "media-jam",
  65064: "media-jam",
};

const codes = {
  // ---------
  // get codes
  // ---------
  /*
  ->
  @PJL DINQUIRE REMAINTONER
  96
   */
  inklevel: () => `${ascii.ESC}%-12345X@PJL${ascii.CR}${ascii.LF}@PJL DINQUIRE REMAINTONER${ascii.CR}${ascii.LF}${ascii.ESC}%-12345X`,
  /*
  ->
  @PJL DINQUIRE NEXTCAREDRM
  11908
   */
  drumleft: () => `${ascii.ESC}%-12345X@PJL${ascii.CR}${ascii.LF}@PJL DINQUIRE NEXTCAREDRM${ascii.CR}${ascii.LF}${ascii.ESC}%-12345X`,

  /*
  ->
  @PJL INFO PAGES
  [6 ENUMERATED]
	A4/LETTER	92
	LEGAL/FOLIO	0
	B5/EXECUTIVE	0
	ENVELOPE	0
	A5	0
	OTHER	0
   */
  pages: () => `${ascii.ESC}%-12345X@PJL${ascii.CR}${ascii.LF}@PJL INFO PAGES${ascii.CR}${ascii.LF}${ascii.ESC}%-12345X`,

  /*
  ->
  @PJL INFO STATUS
  CODE=62122
  DISPLAY="Paper Low Tray 1"
  ONLINE=TRUE
   */
  status: () => `${ascii.ESC}%-12345X@PJL${ascii.CR}${ascii.LF}@PJL INFO STATUS${ascii.CR}${ascii.LF}${ascii.ESC}%-12345X`,

  /*
  ->
  @PJL INFO MULTISTATUS
  CODE=62122
  CODE=40000
  DISPLAY="Paper Low Tray 1"
  ONLINE=TRUE
   */
  multistatus: () => `${ascii.ESC}%-12345X@PJL${ascii.CR}${ascii.LF}@PJL INFO MULTISTATUS${ascii.CR}${ascii.LF}${ascii.ESC}%-12345X`,

  // ---------
  // set codes
  // ---------
  tray: (source: 'auto' | number) => `${ascii.ESC}%-12345X@PJL${ascii.CR}${ascii.LF}@PJL DEFAULT SOURCETRAY=TRAY${source === 'auto' ? 0 : source}${ascii.CR}${ascii.LF}${ascii.ESC}%-12345X`,
  autosleep: (enabled: boolean) => `${ascii.ESC}%-12345X@PJL${ascii.CR}${ascii.LF}@PJL DEFAULT AUTOSLEEP=${enabled ? 'ON' : 'OFF'}${ascii.CR}${ascii.LF}${ascii.ESC}%-12345X`,
  deepsleep: (enabled: boolean) => `${ascii.ESC}%-12345X@PJL${ascii.CR}${ascii.LF}@PJL DEFAULT DEEPSLEEP=${enabled ? 'ON' : 'OFF'}${ascii.CR}${ascii.LF}${ascii.ESC}%-12345X`,
  pagenotify: (enabled: boolean) => `${ascii.ESC}%-12345X@PJL${ascii.CR}${ascii.LF}@PJL USTATUS PAGE=${enabled ? 'ON' : 'OFF'}${ascii.CR}${ascii.LF}${ascii.ESC}%-12345X`,
  jobnotify: (enabled: boolean) => `${ascii.ESC}%-12345X@PJL${ascii.CR}${ascii.LF}@PJL USTATUS JOB=${enabled ? 'ON' : 'OFF'}${ascii.CR}${ascii.LF}${ascii.ESC}%-12345X`,
};


const DIRECTIVES = {
  inklevel: {
    code: codes.inklevel,
    decode: data => {
      const matched = data.toString().match(/@PJL DINQUIRE REMAINTONER[\r\n]*([\d]+)/);
      if (matched) {
        return parseInt(matched[1]);
      }
    }
  },

  drumleft: {
    code: codes.drumleft,
    decode: data => {
      const matched = data.toString().match(/@PJL DINQUIRE NEXTCAREDRM[\r\n]*([\d]+)/);
      if (matched) {
        return parseInt(matched[1]);
      }
    }
  },

  multistatus: {
    code: codes.multistatus,
    decode: data => {
      const matched = data.toString().match(/@PJL INFO MULTISTATUS/);
      if (matched) {
        return kvlist(data, 1);
      }
    }
  }
};

const STATUS_DIRECTIVES = {
  inklevel: {
    code: codes.inklevel,
    decode: data => {
      const matched = data.toString().match(/@PJL DINQUIRE REMAINTONER[\r\n]*([\d]+)/);
      if (matched) {
        return parseInt(matched[1]);
      }
    }
  },

  drumleft: {
    code: codes.drumleft,
    decode: data => {
      const matched = data.toString().match(/@PJL DINQUIRE NEXTCAREDRM[\r\n]*([\d]+)/);
      if (matched) {
        return parseInt(matched[1]);
      }
    }
  },

  state: {
    code: codes.multistatus,
    decode: data => {
      const matched = data.toString().match(/@PJL INFO MULTISTATUS/);
      if (matched) {
        const result = kvlist(data, 1);

        let codes = result['code'];
        if (!Array.isArray(codes)) {
          codes = [codes];
        }

        return {
          reasons: codes.map(code => STATES[code] || code),
          display: result['display'],
        };
      }
    }
  }
};

export class PJL implements Driver {
  public static readonly manufacturer = 'Brother';

  // collected by jiangrui
  public static readonly models = [
    ...["HL-1118","HL-1208","HL-1218W","HL-1850","HL-1870N"], // HL-1
    'HL-2*',
    'HL-5*',
    'HL-6*',
    'HL-B2*',
    'DCP-1*',
    'DCP-7*',
    'DCP-8*',
    'DCP-B7*',
    'MFC-1*',
    'MFC-7*',
    'MFC-8*',
    'MFC-B7*',
    'FAX-2*',
  ];

  constructor(protected transport: Transport) {
  }

  async fetch(cmds: { [name: string]: Directive<any> }): Promise<Props> {
    const result = {};
    const names = Object.keys(cmds);
    const conn = await this.transport.connect();
    let resolved = false;
    try {
      const polling = conn.poll((data, resolve) => {
        if (!data || !data.length) return;
        for (const name of names) {
          const decode = cmds[name].decode;
          if (decode) {
            const decoded = decode(data);
            if (decoded != null) {
              result[name] = decoded;
              names.splice(names.indexOf(name), 1);
              break;
            }
          }
        }

        if (!names.length && !resolved) {
          resolved = true;
          resolve(result);
        }
      });

      for (const name of names) {
        await conn.write(cmds[name].code());
      }

      await polling;

      return result;
    } catch (e) {
      throw e;
    } finally {
      await conn.close();
    }
  }

  async rawstatus(): Promise<Props> {
    return await this.fetch(DIRECTIVES);
  }

  async status(): Promise<Status> {
    return <Status> await this.fetch(STATUS_DIRECTIVES);
  }

}
