import * as minimatch from "minimatch"
import {CDI, DriverClass} from "../defines";
import {parseUri} from "../utils";

const DriverClasses: DriverClass[] =  [
  ...require('./brother')
];

export default DriverClasses;

export function findDriver(uri: string | CDI): DriverClass | undefined {
  const cdi = typeof uri === 'string' ? parseUri(uri) : uri;
  if (!cdi) return;

  return DriverClasses.find(DC => {
    if (DC.manufacturer.toLowerCase() !== cdi.manufacturer) {
      return false;
    }
    const supported = Array.isArray(DC.models) ? DC.models : [DC.models];
    for (const m of supported) {
      if (minimatch(cdi.product, m)) {
        return true;
      }
    }
    return false;
  });
}
