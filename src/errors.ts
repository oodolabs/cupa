import * as errs from 'errs';

export function ERR_DEVICE_NOT_FOUND(identifier) {
  return errs.create({
    code: 'DEVICE_NOT_FOUND',
    status: 404,
    statusCode: 404,
    message: `Device not found${identifier ? ' ' + identifier : '.'}`
  })
}
export function ERR_DIRECTIVE_NOT_SUPPORT(directive) {
  return errs.create({
    code: 'NOT_SUPPORT',
    status: 40011,
    statusCode: 40011,
    message: `The directive \`${directive}\` is no supported`
  })
}

//
//   DRIVER_NOT_FOUND: function (makeAndModel: string) {
//     return errs.create({
//       code: 'DRIVER_NOT_FOUND',
//       status: 40001,
//       statusCode: 40001,
//       message: `No driver found for makeAndModel:${makeAndModel}.`
//     })
//   },
//
//   STATE_NOT_SUPPORT: function (state: string) {
//     return errs.create({
//       code: 'STATE_NOT_SUPPORT',
//       status: 40002,
//       statusCode: 40002,
//       message: `State:${state} not supported.`
//     })
//   },
//
//   USB_NOT_FOUND: function (serial: string) {
//     return errs.create({
//       code: 'USB_NOT_FOUND',
//       status: 40003,
//       statusCode: 40003,
//       message: `USB with serial:${serial} not found.`
//     })
//   },
//   CONNECTION_TYPE_NOT_SUPPORT: function (connectType: string | null) {
//     return errs.create({
//       code: 'CONNECTION_TYPE_NOT_SUPPORT',
//       status: 40004,
//       statusCode: 40004,
//       message: `Connection type ${connectType} not support.`
//     })
//   },
//
//   NOT_NULLABLE: function (value: string) {
//     return errs.create({
//       code: 'NOT_NULLABLE',
//       status: 10001,
//       statusCode: 10001,
//       message: `value ${value} can not be null.`
//     })
//   },
// }
