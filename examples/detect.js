const {detect} = require("..");

(async () => {
  console.log(await detect('usb://Hewlett-Packard/HP%20LaserJet%20Professional%20M1136%20MFP?serial=000000000QHC3CNLPR1a'));
  console.log(await detect('usb://Brother/HL-2595DW?serial=E78125A8N267473'));
})();

