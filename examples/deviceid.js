const {acquireTransport} = require("../lib/transports");

(async () => {
  const transport = acquireTransport('usb://Brother/HL-2595DW?serial=E78125A8N267473');
  if (!transport) return;
  const id = await transport.fetchDeviceId();
  console.log(id);
})();

