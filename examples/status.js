const {createAssistant} = require("../lib");

(async () => {
  const assistant = createAssistant('usb://Brother/HL-2595DW?serial=E78125A8N267473');
  // const assistant = createAssistant('usb://Brother/DCP-1618W?serial=E74215E7N785260');

  // setInterval(async () => {
    // console.log('- inklevel');
    // console.log(await assistant.inklevel());
    // console.log('- drumleft');
    // console.log(await assistant.drumleft());
    // console.log('- papers');
    // console.log(await assistant.papers());
    console.log('- status');
    // console.log('-', await assistant.rawstatus());
    console.log('-', await assistant.status());
    console.log('-----------------')
  // }, 5000);

})();

